import React, { createContext, useEffect, useState } from "react";
import { getUserAccount } from "../services";
import { toast } from "react-toastify";
import { useLocation } from "react-router-dom";
const UserContext = createContext(null);

function UserProvider({ children }) {
  const [user, setUser] = useState({
    isAuthenticated: false,
    token: "",
    account: {},
  });

  const loginContext = (userData) => {
    setUser(userData);
  };

  const logout = () => {
    setUser((user) => ({
      name: "",
      auth: false,
    }));
  };

  const fetchUser = async () => {
    let data = await getUserAccount();
    if (data && data.status === 200) {
      // let roles = data.data.roles;
      // let email = data.data.email;
      // let name = data.data.name;
      // let token = data.data.access_token;
      // toast.success(data.data.message);
      // let data = {
      //   isAuthenticated: true,
      //   token: token,
      //   account: { roles, email, name },
      // };
      setUser(data.data);
    }
    console.log(data.data, "dataaaaa");
  };

  useEffect(() => {
    // if (window.location.pathname === "/") {
    fetchUser();
    // }
  }, []);

  return (
    <UserContext.Provider value={{ user, loginContext, logout }}>
      {children}
    </UserContext.Provider>
  );
}

export { UserContext, UserProvider };
