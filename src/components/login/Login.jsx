import { useContext, useEffect, useState } from "react";
import { Button, Col, Container, Form, Row } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import { loginUser } from "../../services";
import { UserContext } from "../../context/UseContext";

const Login = () => {
  const navigate = useNavigate();

  const [valueLogin, setValueLogin] = useState("");
  const [passwork, setPasswork] = useState("");
  const { loginContext } = useContext(UserContext);

  const handleCreateAccount = () => {
    navigate("/register");
  };

  const handleLogin = async () => {
    if (!valueLogin) {
      toast.error("Email , Phone is required");
      return false;
    }
    if (!passwork) {
      toast.error("passwork  is required");
      return false;
    }

    let res = await loginUser(valueLogin, passwork);

    if (res && res.status === 200) {
      let roles = res.data.roles;
      let email = res.data.email;
      let name = res.data.name;
      let token = res.data.access_token;
      toast.success(res.data.message);
      let data = {
        isAuthenticated: true,
        token: token,
        account: { roles, email, name },
      };
      sessionStorage.setItem("account", JSON.stringify(data));
      // localStorage.setItem("jwt", token);
      loginContext(data);
      navigate("/users");
      // window.location.reload();
    } else {
      toast.error("Error login");
    }

    return true;
  };

  useEffect(() => {
    let session = sessionStorage.getItem("account");
    if (session) {
      navigate("/");
    }
  }, []);

  return (
    <Container>
      <Row>
        <Col lg={6}>Login</Col>
        <Col lg={6}>
          <Form>
            <Form.Group className="mb-3">
              <Form.Label>Email</Form.Label>
              <Form.Control
                type="email"
                placeholder="name@example.com"
                value={valueLogin}
                onChange={(e) => setValueLogin(e.target.value)}
              />
            </Form.Group>
            <Form.Group className="mb-3">
              <Form.Label>Passwork</Form.Label>
              <Form.Control
                type="passwork"
                value={passwork}
                onChange={(e) => setPasswork(e.target.value)}
              />
            </Form.Group>
            <Button variant="primary" onClick={() => handleLogin()}>
              login
            </Button>
            <Button
              variant="primary"
              type="submit"
              onClick={() => handleCreateAccount()}
            >
              Create new account
            </Button>
          </Form>
        </Col>
      </Row>
    </Container>
  );
};

export default Login;
