import { useEffect, useState } from "react";
import { Container, Form } from "react-bootstrap";
import { toast } from "react-toastify";
import { fetchGroup } from "../../services";

const GroupRoles = () => {
  const [userGroup, setUserGroup] = useState([]);
  const [selectGroup, setSelectGroup] = useState("");

  useEffect(() => {
    fetchUsersGroup();
  }, []);

  const fetchUsersGroup = async () => {
    let res = await fetchGroup();
    if (res && res.status === 200) {
      setUserGroup(res?.data?.results);
    } else {
      toast.error("error");
    }
  };

  const handleOnChangeSelect = () => {};

  return (
    <Container>
      <div>
        <h2>Group roles</h2>
        <div>Select group</div>
      </div>
      <Form.Select aria-label="Default select example">
        <option>Open this select menu</option>
        {userGroup?.length > 0 &&
          userGroup?.map((item, index) => {
            return (
              <option value={item.id} key={`index${index}`}>
                {item.name}
              </option>
            );
          })}
      </Form.Select>
    </Container>
  );
};
export default GroupRoles;
