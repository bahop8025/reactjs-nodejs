import { useState } from "react";
import { Button, Col, Container, Form, Row } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { registerUser } from "../../services";

const Register = () => {
  const navigate = useNavigate();
  const [email, setEmail] = useState("");
  const [phone, setPhone] = useState("");
  const [password, setPasswork] = useState("");
  const [confimPasswork, setConfimPasswork] = useState("");
  const [name, setName] = useState("");
  const handleLogin = () => {
    navigate("/login");
  };

  const isValidate = () => {
    if (!email) {
      toast.success("Email is required");
      return false;
    }
    if (!phone) {
      toast.success("Phone is required");
      return false;
    }
    if (!name) {
      toast.success("Name is required");
      return false;
    }
    if (!password) {
      toast.success("Password is required");
      return false;
    }
    if (password !== confimPasswork) {
      toast.success("ConfimPasswork is required");
      return false;
    }

    return true;
  };

  const handleRegister = () => {
    let check = isValidate();

    if (check == true) {
      registerUser(email, password, phone, name);
      navigate("/login");
    }
  };

  return (
    <Container>
      <Row>
        <Col lg={6}>Register</Col>
        <Col lg={6}>
          <Form>
            <Form.Group className="mb-3">
              <Form.Label>Name</Form.Label>
              <Form.Control
                type="email"
                placeholder=""
                value={name}
                onChange={(e) => setName(e.target.value)}
              />
            </Form.Group>
            <Form.Group className="mb-3">
              <Form.Label>Email</Form.Label>
              <Form.Control
                type="text"
                placeholder="name@example.com"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
            </Form.Group>
            <Form.Group className="mb-3">
              <Form.Label>Phone</Form.Label>
              <Form.Control
                type="text"
                value={phone}
                onChange={(e) => setPhone(e.target.value)}
              />
            </Form.Group>
            <Form.Group className="mb-3">
              <Form.Label>Passwork</Form.Label>
              <Form.Control
                type="password"
                value={password}
                onChange={(e) => setPasswork(e.target.value)}
              />
            </Form.Group>
            <Form.Group className="mb-3">
              <Form.Label>Confim Passwork</Form.Label>
              <Form.Control
                type="password"
                value={confimPasswork}
                onChange={(e) => setConfimPasswork(e.target.value)}
              />
            </Form.Group>
            <Button variant="primary" onClick={() => handleLogin()}>
              login
            </Button>
            <Button variant="primary" onClick={() => handleRegister()}>
              Register
            </Button>
          </Form>
        </Col>
      </Row>
    </Container>
  );
};

export default Register;
