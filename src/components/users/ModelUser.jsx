import { useEffect, useState } from "react";
import { Button, Col, Form, InputGroup, Modal, Row } from "react-bootstrap";
import {
  ReadUser,
  createNewUser,
  fetchGroup,
  updateNewUser,
} from "../../services";
import { toast } from "react-toastify";
import _ from "lodash";

const ModelUser = (props) => {
  const defaultValue = {
    email: "",
    password: "",
    phone: "",
    address: "",
    name: "",
    sex: "",
    city: "",
    group_id: "",
  };
  const [userGroup, setUserGroup] = useState([]);
  const [userData, setUserData] = useState(defaultValue);

  useEffect(() => {
    fetchUsersGroup();
  }, []);

  useEffect(() => {
    if (props.action === "UPDATE") {
      setUserData(props.dataModelUser);
    }
  }, [props.dataModelUser]);

  const handelOnchangeInput = (value, name) => {
    let _userData = _.cloneDeep(userData);
    _userData[name] = value;
    setUserData(_userData);

    return;
  };

  const fetchUsersGroup = async () => {
    let res = await fetchGroup();
    if (res && res.status === 200) {
      setUserGroup(res?.data?.results);
    } else {
      toast.error("error");
    }
  };

  const handelSubmit = async () => {
    try {
      let res =
        props.action === "CREATE"
          ? await createNewUser(userData)
          : await updateNewUser(userData);

      if (res && res.status === 200) {
        props.onHide();
        props.action === "CREATE"
          ? toast.success("User created successfully")
          : toast.success("User Update successfully");
      } else {
        props.action === "CREATE"
          ? toast.error("Error creating user")
          : toast.error("Error Update user");
      }
    } catch (error) {
      console.error("Error  user:", error);
      props.action === "CREATE"
        ? toast.error("Error creating user")
        : toast.error("Error Update user");
    }
  };

  return (
    <div>
      <Modal size="lg" show={props.show} centered onHide={props.onHide}>
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            {props.action === "CREATE" ? "Create new user" : "Edit user"}
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div>
            <Row>
              <Col lg={6}>
                <InputGroup size="sm" className="mb-3">
                  <InputGroup.Text id="inputGroup-sizing-sm">
                    Email
                  </InputGroup.Text>
                  <Form.Control
                    disabled={props.action === "CREATE" ? false : true}
                    aria-label="email"
                    aria-describedby="inputGroup-sizing-sm"
                    value={userData.email || ""}
                    onChange={(event) =>
                      handelOnchangeInput(event.target.value, "email")
                    }
                  />
                </InputGroup>
                <InputGroup size="sm" className="mb-3">
                  <InputGroup.Text id="inputGroup-sizing-sm">
                    Name
                  </InputGroup.Text>
                  <Form.Control
                    aria-label="name"
                    aria-describedby="inputGroup-sizing-sm"
                    value={userData.name || ""}
                    onChange={(event) =>
                      handelOnchangeInput(event.target.value, "name")
                    }
                  />
                </InputGroup>
              </Col>
              <Col lg={6}>
                <InputGroup size="sm" className="mb-3">
                  <InputGroup.Text id="inputGroup-sizing-sm">
                    Phone
                  </InputGroup.Text>
                  <Form.Control
                    disabled={props.action === "CREATE" ? false : true}
                    aria-label="phone"
                    aria-describedby="inputGroup-sizing-sm"
                    value={userData.phone || ""}
                    onChange={(event) =>
                      handelOnchangeInput(event.target.value, "phone")
                    }
                  />
                </InputGroup>
                {props.action === "CREATE" && (
                  <InputGroup size="sm" className="mb-3">
                    <InputGroup.Text id="inputGroup-sizing-sm">
                      Password
                    </InputGroup.Text>
                    <Form.Control
                      aria-label="Password"
                      aria-describedby="inputGroup-sizing-sm"
                      value={userData.password || ""}
                      onChange={(event) =>
                        handelOnchangeInput(event.target.value, "password")
                      }
                    />
                  </InputGroup>
                )}
              </Col>
              <InputGroup size="sm" className="mb-3">
                <InputGroup.Text id="inputGroup-sizing-sm">
                  Address
                </InputGroup.Text>
                <Form.Control
                  aria-label="address"
                  aria-describedby="inputGroup-sizing-sm"
                  value={userData.address || ""}
                  onChange={(event) =>
                    handelOnchangeInput(event.target.value, "address")
                  }
                />
              </InputGroup>
              <InputGroup size="sm" className="mb-3">
                <InputGroup.Text id="inputGroup-sizing-sm">
                  City
                </InputGroup.Text>
                <Form.Control
                  aria-label="city"
                  aria-describedby="inputGroup-sizing-sm"
                  value={userData.city || ""}
                  onChange={(event) =>
                    handelOnchangeInput(event.target.value, "city")
                  }
                />
              </InputGroup>

              <Col lg={6}>
                <Form.Select
                  aria-label="Default select example"
                  value={userData.sex || 1}
                  onChange={(event) =>
                    handelOnchangeInput(event.target.value, "sex")
                  }
                >
                  <option>Open this select menu</option>
                  <option value="0">Male</option>
                  <option value="1">Female</option>
                </Form.Select>
              </Col>
              <Col lg={6}>
                <Form.Select
                  aria-label="Default select example"
                  value={userData.group_id || 1}
                  onChange={(event) =>
                    handelOnchangeInput(event.target.value, "group_id")
                  }
                >
                  <option>Open this select menu</option>
                  {userGroup?.length > 0 &&
                    userGroup?.map((item, index) => {
                      return (
                        <option value={item.id} key={`index${index}`}>
                          {item.name}
                        </option>
                      );
                    })}
                </Form.Select>
              </Col>
            </Row>
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={props.onHide}>
            Close
          </Button>
          <Button variant="primary" onClick={handelSubmit}>
            {props.action === "CREATE" ? "Save" : "Update"}
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
};

export default ModelUser;
