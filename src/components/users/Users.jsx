import { useEffect, useState } from "react";
import { Button, Table } from "react-bootstrap";
import { toast } from "react-toastify";
import { ReadUser, deleteUser } from "../../services";
import Model from "./Model";
import ModelUser from "./ModelUser";

const Users = () => {
  const [listUsers, setListUsers] = useState([]);
  const [isShowModel, setShowModel] = useState(false);
  const [dataUser, setDataUser] = useState({});
  const [isShowModelUser, setShowModelUser] = useState(false);
  const [action, setAction] = useState("CREATE");
  const [dataModelUser, setDataModelUser] = useState({});

  useEffect(() => {
    fetchUsers();
  }, []);

  const fetchUsers = async () => {
    let res = await ReadUser();
    setListUsers(res?.data?.data);
  };

  const handleDeleteUser = (id) => {
    setShowModel(true);
    setDataUser(id);
    setAction("CREATE");
  };

  const handleClose = () => {
    setShowModel(false);
    setDataUser({});
  };

  const onHideModelUser = () => {
    setShowModelUser(false);
    setDataModelUser({});
  };

  const handConfirmDelete = async () => {
    let res = await deleteUser(dataUser);
    if (res && res.status === 200) {
      toast.success("Delete user successfully");
      let res = await ReadUser();
      setListUsers(res?.data?.data);
      setShowModel(false);
    } else {
      toast.error("Error deleting user");
    }
  };

  const handleEditUser = (user) => {
    setDataModelUser(user);
    setShowModelUser(true);
    setAction("UPDATE");
  };

  return (
    <>
      <div>
        <Button
          variant="info"
          onClick={() => {
            setShowModelUser(true);
            setAction("CREATE");
          }}
        >
          Add new
        </Button>
        <Button variant="secondary">Secondary</Button>
      </div>
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>id</th>
            <th>Name</th>
            <th>Phone</th>
            <th>Email</th>
            <th>Group</th>
            <th>#</th>
          </tr>
        </thead>
        <tbody>
          {listUsers?.length > 0 &&
            listUsers?.map((user, index) => (
              <tr key={`user${index}`}>
                <td>{user?.id}</td>
                <td>{user?.name}</td>
                <td>{user?.phone}</td>
                <td>{user?.email}</td>
                <td>{user?.Column10 ? user?.Column10 : ""}</td>
                <td>
                  <Button
                    variant="danger"
                    onClick={() => handleDeleteUser(user.id)}
                  >
                    Delete
                  </Button>
                  <Button variant="dark" onClick={() => handleEditUser(user)}>
                    Edit
                  </Button>
                </td>
              </tr>
            ))}
        </tbody>
      </Table>

      <Model
        show={isShowModel}
        handleClose={handleClose}
        handConfirmDelete={handConfirmDelete}
        dataUser={dataUser}
      />

      <ModelUser
        show={isShowModelUser}
        onHide={onHideModelUser}
        action={action}
        dataModelUser={dataModelUser}
      />
      {/* <ReactPaginate
        nextLabel="next >"
        onPageChange={handlePageClick}
        pageRangeDisplayed={3}
        marginPagesDisplayed={2}
        pageCount={30}
        previousLabel="< previous"
        pageClassName="page-item"
        pageLinkClassName="page-link"
        previousClassName="page-item"
        previousLinkClassName="page-link"
        nextClassName="page-item"
        nextLinkClassName="page-link"
        breakLabel="..."
        breakClassName="page-item"
        breakLinkClassName="page-link"
        containerClassName="pagination"
        activeClassName="active"
        renderOnZeroPageCount={null}
      /> */}
    </>
  );
};

export default Users;
