import { useContext, useEffect, useState } from "react";
import { Container, Nav, NavDropdown, Navbar } from "react-bootstrap";
import { UserContext } from "../../context/UseContext";
import { logOutUser } from "../../services";
import { toast } from "react-toastify";

const Navigation = () => {
  const [isShow, setIsShow] = useState(false);
  const { user } = useContext(UserContext);
  // console.log(user);
  useEffect(() => {
    let session = sessionStorage.getItem("account");
    if (session) {
      setIsShow(true);
    }
  }, []);

  const heandLogOut = async () => {
    let data = await logOutUser();
    console.log(data);
    if (data && data.data === 200) {
      toast.success("Logout successfully");
      localStorage.removeItem("jwt");
      console.log("cc");
    }
  };

  return (
    <>
      {isShow && (
        <Navbar expand="lg" className="bg-body-tertiary">
          <Container fluid>
            <Navbar.Brand href="#">Navbar</Navbar.Brand>
            <Navbar.Toggle aria-controls="navbarScroll" />
            <Navbar.Collapse id="navbarScroll">
              <Nav
                className="me-auto my-2 my-lg-0"
                style={{ maxHeight: "100px" }}
                navbarScroll
              >
                <Nav.Link href="/home">Home</Nav.Link>
                <Nav.Link href="/users">User</Nav.Link>
                <Nav.Link href="/roles">Roles</Nav.Link>
                <Nav.Link href="/group-roles">Group-Roles</Nav.Link>
                <Nav.Link href="/project">Project</Nav.Link>
                <Nav.Link href="/about">About</Nav.Link>
              </Nav>
              {/* {user && user.isAuthenticated === true &&  <Nav.Item>ccc</Nav.Item>} */}
              <Nav.Item>ccc</Nav.Item>
              <NavDropdown title="Setting" id="collapsible-nav-dropdown">
                {/* <NavDropdown.Item> */}
                <button onClick={() => heandLogOut()}>LogOut</button>
                {/* </NavDropdown.Item> */}
              </NavDropdown>
            </Navbar.Collapse>
          </Container>
        </Navbar>
      )}
    </>
  );
};

export default Navigation;
