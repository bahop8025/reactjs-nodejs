import _ from "lodash";
import { useState } from "react";
import { Button, Col, Container, Form, InputGroup, Row } from "react-bootstrap";
import { toast } from "react-toastify";
import { v4 as uuidv4 } from "uuid";
import { createRoles } from "../../services";

const Roles = () => {
  const [listChilds, setListChilds] = useState({
    child1: { url: "", description: "" },
  });

  const handleOnchangeInput = (name, value, key) => {
    let _listChilds = _.cloneDeep(listChilds);
    _listChilds[key][name] = value;
    setListChilds(_listChilds);
  };

  const handleAddNewChild = () => {
    let _listChilds = _.cloneDeep(listChilds);
    _listChilds[`child-${uuidv4()}`] = { url: "", description: "" };
    setListChilds(_listChilds);
  };

  const handleDeleteChild = (key) => {
    let _listChilds = _.cloneDeep(listChilds);
    delete _listChilds[key];
    setListChilds(_listChilds);
  };

  const builData = () => {
    let _listChilds = _.cloneDeep(listChilds);
    let result = [];
    Object.entries(_listChilds).map(([key, value], index) => {
      result.push({
        url: value.url,
        description: value.description,
      });
    });
    return result;
  };

  const handleSave = async () => {
    let check = true;
    let imvalite = Object.entries(listChilds).find(([key, value], index) => {
      return value && !value.url;
    });

    if (!imvalite) {
      let data = builData();
      let res = await createRoles(data);
      console.log(res);
    } else {
      toast.error("Url required");
    }
  };

  return (
    <Container>
      <div className="mt-5">
        {Object.entries(listChilds).map(([key, value], index) => {
          return (
            <Row key={`child${key}`} className={`${key}`}>
              <Col lg={5}>
                <InputGroup className="mb-3">
                  <InputGroup.Text id="inputGroup-sizing-sm">
                    URL
                  </InputGroup.Text>
                  <Form.Control
                    value={value.url}
                    onChange={(event) =>
                      handleOnchangeInput("url", event.target.value, key)
                    }
                    aria-label="url"
                    aria-describedby="inputGroup-sizing-sm"
                  />
                </InputGroup>
              </Col>
              <Col lg={5}>
                <InputGroup className="mb-3">
                  <InputGroup.Text id="inputGroup-sizing-sm">
                    Description
                  </InputGroup.Text>
                  <Form.Control
                    value={value.description}
                    onChange={(event) =>
                      handleOnchangeInput(
                        "description",
                        event.target.value,
                        key
                      )
                    }
                    aria-label="description"
                    aria-describedby="inputGroup-sizing-sm"
                  />
                </InputGroup>
              </Col>
              <Col lg={2}>
                <Button variant="primary" onClick={() => handleAddNewChild()}>
                  +
                </Button>{" "}
                {index >= 1 && (
                  <Button
                    variant="danger"
                    onClick={() => handleDeleteChild(key)}
                  >
                    x
                  </Button>
                )}
              </Col>
            </Row>
          );
        })}
      </div>

      <Row>
        <Col lg={5}>
          <Button variant="info" onClick={() => handleSave()}>
            Save
          </Button>
        </Col>
      </Row>
    </Container>
  );
};
export default Roles;
