import axios from "../setup/axios";

const registerUser = async (email, password, phone, name) => {
  return await axios.post("api/v1/create-users", {
    email,
    password,
    phone,
    name,
  });
};

const loginUser = async (valueLogin, passwork) => {
  return await axios.post("api/v1/login", {
    valueLogin,
    passwork,
  });
};

const ReadUser = async () => {
  return await axios.get("api/v1/users");
};

const deleteUser = async (id) => {
  return await axios.delete(`api/v1/delete-users/${id}`);
};

const fetchGroup = async () => {
  return await axios.get(`api/v1/group/read`);
};

const createNewUser = async (data) => {
  return await axios.post(`api/v1/create-users`, {
    ...data,
  });
};

const updateNewUser = async (data) => {
  console.log(data, "update");
  return await axios.put(`api/v1/update-users`, {
    ...data,
  });
};

const getUserAccount = async () => {
  return await axios.get(`api/v1/account`);
};

const logOutUser = async () => {
  return await axios.post(`api/v1/logout`);
};

const createRoles = async (roles) => {
  return await axios.post(`api/v1/roles/create`, [...roles]);
};

export {
  registerUser,
  loginUser,
  ReadUser,
  deleteUser,
  fetchGroup,
  createNewUser,
  updateNewUser,
  getUserAccount,
  logOutUser,
  createRoles,
};
