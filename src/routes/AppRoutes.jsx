import { useContext, useEffect } from "react";
import { Route, Routes, useNavigate } from "react-router-dom";
import NotFound from "../components/404";
import HomePage from "../components/Home";
import Login from "../components/login/Login";
import Register from "../components/register/Register";
import Users from "../components/users/Users";
import Project from "../components/project/Project";
import { UserContext } from "../context/UseContext";
import Roles from "../components/roles/Roles";
import GroupRoles from "../components/groupRoles/groupRoles";

const AppRoutes = (props) => {
  const navigate = useNavigate();
  const { user } = useContext(UserContext);
  useEffect(() => {
    let session = sessionStorage.getItem("account");
    if (!session) {
      navigate("/login");
    }
  }, []);

  return (
    <>
      <Routes>
        <Route path="/" element={<HomePage />} />
        <Route path="/login" element={<Login />} />
        <Route path="/project" element={<Project />} />
        <Route path="/roles" element={<Roles />} />
        <Route path="/group-roles" element={<GroupRoles />} />
        <Route path="/users" element={<Users />} />
        <Route path="/register" element={<Register />} />
        <Route path="*" element={<NotFound />} />
      </Routes>
    </>
  );
};

export default AppRoutes;
